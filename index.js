


const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

//document.getElementById('txt-first-name')
//document.getElementsByClassName('txt-inputs')
//document.getElementsByTagName('input')


const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;


	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName)


// txtFirstName.addEventListener('keyup' , (e) => {
// 	console.log(e.target)
// 	console.log(e.target.value)
// })


//Mini Activity
	//Listen to an event when the last name's input is changed.